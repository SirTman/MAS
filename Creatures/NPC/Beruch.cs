﻿using MAS.Actions;
using MAS.FormAdditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAS
{
    class Beruch : Character
    {
        public Beruch() {
            this.Name = "Beruch";
            this.Species = "Goblin";
            
            this.body.Height = 91.0;//cm
            this.body.Feminity = 30;
            this.body.Weight = 1.0;
            this.body.Thickness = 30;

            this.body.SetAllBustSize(6);
            this.body.CreateVagina();
            this.body.AssVol = 6;
            this.body.ThighsHips = 7;

            this.body.Skin = "skin";
            this.body.SkinTone = "green";
            this.body.SkinAdjective = "oily";

            this.face.HairColour = "auburn";
            this.face.HairStyle = "spiky";
            this.face.HairLength = 13;
            this.face.SetEyeColour("red");

            this.face.Ears = RacialType.ELVISH;
            this.face.EarsPier = "Pair of Silver Studs";

            this.StrengthScore = 8;
            this.ReflexScore = 14;
            this.ConstitutionScore = 10;
            this.CunningScore = 8;
            this.PresenceScore = 11;
            this.IntellectScore = 10;

            this.Reputation = 2;

            this.Long = String.Format($"Beruch is not especially tall or strong looking. The little munchkin is simply tenacious and skilled. She holds her blade with firm grips, a variety of pouches and tools strapped around her waist. Her stance is steady and indicative of a long history of fighting in this arena. Her open leather blouse struggle to contain the entirety of her heavy {this.body.GetBiggestBust()}, keeping it compressed and out of the way.");
        }

        public void FirstMeeting() {
            Long = "You wanna fight huh!!";
            NextCombat = new Combat(this);
            Scene.SetButtons(ButtonFactory.AddButton("Fight", StartCombat));
            Scene.AddButtons(ButtonFactory.AddButton("Run", MAS.MainForm.ReturnToWorkshop));
            //Scene.AddButtons(ButtonFactory.AddButton("DB: Remove Beruch", RunGoblin));
            //Scene.Buttonlist.Add(ButtonFactory.AddButton("Fight", fight.StartCombat()));
        }

        protected void StartCombat(object sender, EventArgs e)
        {
            NextCombat.StartCombat();
        }

        protected void RunGoblin(object sender, EventArgs e)
        {
            NextCombat.RemoveFoe(this);
        }

        public void DrainThosePuppies() {
            this.Long = String.Format($"You watch as the little imp is strapped with leather binds into a chair.");
        }

        public override void CombatAI(Player player, List<Character> creatures) {
            Attack(player);
        }

        private void Attack(Character creature) {
            WeaponAttack atk = new WeaponAttack(this);
            Scene.Output = "The Little Goblin comes up and tries to slam you with her chest..\n";
            if (creature.Hit(atk)) Scene.Output += $"({atk.ToHit}) She hits you for {atk.DmgHeal} {atk.DmgType}";
            else Scene.Output += $"({atk.ToHit}) She misses you and falls flat on the ground.";

        }
    }
}
