﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS;
using MAS.Rooms;
using MAS.FormAdditions;
using MAS.Items;
using MAS.Actions;
using System.Windows.Forms;

namespace MAS
{
    public abstract class Character
    {
        private static Random rnd = new Random();
        private string RandomChoice(string[] str)
        {
            int r = rnd.Next(str.Length);
            return str[r];
        }

        public string Name { get; protected set; }
        public Room Scene { get; } = Global.CurrentRoom;
        private string _long;
        public string Long {
            get { return _long; }
            protected set {
                _long = value;
                Scene.Output = value;
            }
        }//Long Description
        public string Species { get; set; }
        public Sex gender { get; protected set; }
        public bool HasFur { get; set; }

        public Face face = new Face();
        public Body body = new Body();

        public object SexualPreferences;

        #region Core Stats
        //private const int baseStat = 10;
        private int _constitutionScore;

        protected int StrengthScore { set; get; } //Strength,
        protected int ReflexScore { set; get; } //Reflex, 
        protected int ConstitutionScore {
            set { _constitutionScore = value; SetHPMax(); }
            get { return _constitutionScore; }
        } //Constitution, 
        protected int CunningScore { set; get; } //Cunning, 
        protected int PresenceScore { set; get; } //Presence,
        protected int IntellectScore { set; get; } //Intellect

        //The Modifier e.g +1, -3, 0 etc.
        private int StatModifier(int score) {
            double scoreD = score;
            score = Convert.ToInt32(Math.Floor((scoreD - 10) / 2));
            return score;
        }
        public int STR { get { return this.StatModifier(StrengthScore); } }
        public int RFX { get { return this.StatModifier(ReflexScore); } }
        public int CON { get { return this.StatModifier(ConstitutionScore); } }
        public int CUN { get { return this.StatModifier(CunningScore); } }
        public int PRE { get { return this.StatModifier(PresenceScore); } }
        public int INT { get { return this.StatModifier(IntellectScore); } }


        ///Condition Stats
        private int _reputation = 1;
        public int Reputation { get; set; }

        private double _libido = 0.0;   //Libido 0-100
        private double _senstivity = 1.0;   //Senstivity Multiplyer x0.0
        public double Senstivity { get; private set; }

        public double Lust { get; set; }    //Lust 
        private double _lustMax = 100.0; //Lust Maximun DFLT: 100
        public double LustMax { get; private set; }

        private int _HP; //Hit Points
        public int HP {
            get { return _HP; }
            protected set {
                _HP = value;
                if (_HP > HPmax)
                    _HP = HPmax;
            }
        } 

        private int _hpMax;   //Hit Points Maximun
        public int HPmax { get { return _hpMax; } }

        public void SetHPMax() {
            _hpMax = _reputation + (this.CON * _reputation);
        }
        public void OverrideHPMax() {

        }
        public bool isAlive { get {
                if (HP <= 0)
                    return false;
                else
                    return true;
            }
        }

        public virtual void UpdateState() {
            foreach (GroupBox gb in MAS.MainForm.foeLayoutPanel.Controls) {
                if (gb.Tag.Equals(this))
                {
                    var hpBar = gb.Controls.OfType<ProgressBar>();
                    foreach (ProgressBar hp in hpBar) {
                        
                        try {
                            hp.Value = this.HP;
                        }
                        catch (ArgumentOutOfRangeException) {
                            hp.Value = 0;
                        }
                    }
                    if (!isAlive) {
                        gb.Text = $"(DEAD) {this.Name}";
                    }
                }
                //TODO
            }
        }

        public string GetGender()
        {
            string t;
            switch (gender) {
                case Sex.male:
                    t = "male";
                    break;
                case Sex.female:
                    t = "female";
                    break;
                case Sex.andromorph:
                    t = "andromorph";
                    break;
                case Sex.gynomorphy:
                    t = "gynomorphy";
                    break;
                case Sex.herm:
                    t = "herm";
                    break;
                case Sex.maleherm:
                    t = "maleherm";
                    break;
                case Sex.undefined:
                    t = "unknown";
                    break;
                default:
                    t = "unknown";
                    break;
            }
            return t;
        }
        public int GetAbilityScore(Stat abl) {
            int score = 0;
            switch (abl) {
                case Stat.STR:
                    score = this.StrengthScore;
                    break;
                case Stat.RFX:
                    score = this.ReflexScore;
                    break;
                case Stat.CON:
                    score = this.ConstitutionScore;
                    break;
                case Stat.CUN:
                    score = this.CunningScore;
                    break;
                case Stat.PRE:
                    score = this.PresenceScore;
                    break;
                case Stat.INT:
                    score = this.IntellectScore;
                    break;
                default:
                    score = 0;
                    break;
            }
            return score;
        }
        public void SetAbilityScore(Stat abl, int num)
        {
            switch (abl) {
                case Stat.STR:
                    this.StrengthScore = num;
                    break;
                case Stat.RFX:
                    this.ReflexScore = num;
                    break;
                case Stat.CON:
                    this.ConstitutionScore = num;
                    SetHPMax();
                    break;
                case Stat.CUN:
                    this.CunningScore = num;
                    break;
                case Stat.PRE:
                    this.PresenceScore = num;
                    break;
                case Stat.INT:
                    this.IntellectScore = num;
                    break;
            }
        }
        public void AddAbilityScore(Stat abl, int num)
        {
            switch (abl) {
                case Stat.STR:
                    this.StrengthScore += num;
                    break;
                case Stat.RFX:
                    this.ReflexScore += num;
                    break;
                case Stat.CON:
                    this.ConstitutionScore += num;
                    SetHPMax();
                    break;
                case Stat.CUN:
                    this.CunningScore += num;
                    break;
                case Stat.PRE:
                    this.PresenceScore += num;
                    break;
                case Stat.INT:
                    this.IntellectScore += num;
                    break;
            }
        }
        public void SubAbilityScore(Stat abl, int num) {
            switch (abl)
            {
                case Stat.STR:
                    this.StrengthScore -= num;
                    break;
                case Stat.RFX:
                    this.ReflexScore -= num;
                    break;
                case Stat.CON:
                    this.ConstitutionScore -= num;
                    SetHPMax();
                    break;
                case Stat.CUN:
                    this.CunningScore -= num;
                    break;
                case Stat.PRE:
                    this.PresenceScore -= num;
                    break;
                case Stat.INT:
                    this.IntellectScore -= num;
                    break;
            }
        }
        //Make Stats Quicker
        public void CreateAbilityScores(int str) {
            this.StrengthScore = str;
        }
        public void CreateAbilityScores(int str, int rfx)
        {
            this.StrengthScore = str;
            this.ReflexScore = rfx;
        }
        public void CreateAbilityScores(int str, int rfx, int con)
        {
            this.StrengthScore = str;
            this.ReflexScore = rfx;
            this.ConstitutionScore = con; SetHPMax();
        }
        public void CreateAbilityScores(int str, int rfx, int con, int cun)
        {
            this.StrengthScore = str;
            this.ReflexScore = rfx;
            this.ConstitutionScore = con; SetHPMax();
            this.CunningScore = cun;
        }
        public void CreateAbilityScores(int str, int rfx, int con, int cun, int pre)
        {
            this.StrengthScore = str;
            this.ReflexScore = rfx;
            this.ConstitutionScore = con; SetHPMax();
            this.CunningScore = cun;
            this.PresenceScore = pre;
        }
        public void CreateAbilityScores(int str, int rfx, int con, int cun, int pre, int intl)
        {
            this.StrengthScore = str;
            this.ReflexScore = rfx;
            this.ConstitutionScore = con; SetHPMax();
            this.CunningScore = cun;
            this.PresenceScore = pre;
            this.IntellectScore = intl;
        }
        # endregion

        #region Inventory
        private List<Item> Inventory = new List<Item>();
        public void AddItem(Item item) {
            try {
                if (!HasFreeInventorySlot) {
                    throw new Exception("Your Inventory is currently full");
                }
                else {
                    MAS.MainForm.TextOutput = $"{item.Name} was added to your inventory";
                    Inventory.Add(item);
                }
            }
            catch (Exception e) {
                MAS.MainForm.TextOutput = e.ToString();
            }

        }

        public void RemoveItem(Item item) {
            try
            {
                if (Inventory.Contains(item)) {
                    Inventory.Remove(item);
                    MAS.MainForm.TextOutput = $"{Environment.NewLine}{item.Name} was removed from your inventory";
                }
                else
                    throw new Exception("You do not have any such item in your inventory");
            }
            catch (Exception e) {
                MAS.MainForm.TextOutput = e.ToString();
            }
        }

        public int InventorySlots { get {
                int slots = 10;
                //TODO: Inv Mods
                return slots;
            }
        }
        public int FreeInventorySlots { get { return (InventorySlots - Inventory.Count()); } }
        public bool HasFreeInventorySlot { get { return (FreeInventorySlots >= InventorySlots); } }
        #endregion

        #region Equipment
        public bool EnableAkimbo { set; get; } = false;

        private Item _mainHand;
        public Item MainHand {
            private set { _mainHand = SwapItem(_mainHand, value); }
            get { return _mainHand; } }

        private Item _offHand;
        public Item OffHand {
            private set { _offHand = SwapItem(_offHand, value); }
            get { return _offHand; }
        }

        private Item _armor;
        public Item Armor {
            private set { _armor = SwapItem(_armor, value); }
            get { return _armor; }
        }
        
        public int Defense { get {
                try { 
                    int ac = (10 + Armor.Defense);
                    if (Armor.Weight == WeightVerb.NOTHING || Armor.Weight == WeightVerb.LIGHT) {
                        ac += RFX;
                        if (Armor.Weight == WeightVerb.NOTHING) ac += Sheild.Defense;
                    }  
                    else if (Armor.Weight == WeightVerb.MEDIUM)
                        ac += (RFX > 2) ? 2 : RFX; //Max 2 for Medium Armor
                    return ac;
                }
                catch {
                    return (10 + RFX);
                }
            } }

        private Item _sheild;
        public Item Sheild {
            private set { _sheild = SwapItem(_sheild, value); }
            get { return _sheild; }
        }

        private Item _upperGarment;
        public Item UpperGarment {
            private set { _upperGarment = SwapItem(_upperGarment, value); }
            get { return _upperGarment; }
        }

        private Item _lowerGarment;
        public Item LowerGarment {
            private set { _lowerGarment = SwapItem(_lowerGarment, value); }
            get { return _lowerGarment; }
        }

        public bool EquipItem(Item NewItem) {
            try {
                switch (NewItem.Type) {
                    case ItemType.Melee_Weapon:
                        MainHand = NewItem;
                        break;
                    case ItemType.Ranged_Weapon:
                        MainHand = NewItem;
                        break;
                    case ItemType.Armor:
                        Armor = NewItem;
                        break;
                    case ItemType.Shield:
                        Sheild = NewItem;
                        break;
                    case ItemType.Upper_Undergarment:
                        UpperGarment = NewItem;
                        break;
                    case ItemType.Lower_Undergarment:
                        LowerGarment = NewItem;
                        break;
                    default:
                        throw new Exception("This Item Can't be equipped");
                }
                return true;
            }
            catch (Exception e) {
                MAS.MainForm.TextOutput = e.ToString();
                return false;
            }
            
        }

        private Item SwapItem(Item equipSlot, Item newItem) {
            try {
                //If Player doesn't have anything equiped.
                if (equipSlot == null) { 
                    Inventory.Remove(newItem);
                    return newItem;
                }
                //If They have Something Equipped.
                else {
                    if (HasFreeInventorySlot) {
                        Inventory.Remove(newItem);
                        Inventory.Add(equipSlot);
                        return newItem;
                    }
                    else throw new Exception("You has no free inventory slots");
                }
            }
            catch (Exception e) {
                MAS.MainForm.TextOutput = e.ToString();
                return equipSlot;
            }
        }
        #endregion

        #region Perks & Traits
        public List<DamageType> Resistances = new List<DamageType>();
        public List<DamageType> Immunity = new List<DamageType>();

        public int HasResistanceOrImmunity() {
            

            return 0;
        }

        public List<Object> Perks; //!Place Holder!


        #endregion

        #region Combat
        protected Combat NextCombat { set; get; }
        public void Damage(int number) {
            HP -= number;
            UpdateState();
        }
        public void Damage(int number, DamageType damageType)
        {
            if (Immunity.Contains(damageType)) {
                Scene.Output = $"{Name} is immune to {damageType.ToString()} damage.";
            }
            else if (Resistances.Contains(damageType) || Immunity == null)
                HP -= (number / 2);
            else
                HP -= number;
            UpdateState();
        }
        /// <summary>Deal X% of Maximum HP </summary>
        /// <param name="number">Whole Percantage</param>
        public void DamageScaled(int number)
        {
            HP -= (number / 100) * HPmax;
        }
        
        public void Heal(int number) {
            if ((HP + number) > HPmax) HP = HPmax;
            else HP += number;
        }
        /// <summary>Heals X% of Maximum HP </summary>
        /// <param name="number">Whole Percantage</param>
        public void HealScaled(int number)
        {
            if ((HP + number) > HPmax) HP = HPmax;
            else HP += number;
        }

        public virtual bool Hit(CombatAction action) {
            if (action.ToHit >= Defense) {
                Damage(action.DmgHeal, action.DmgType);
                return true;
            }
            else
                return false;
        }
        //Core combat AI handling; this methods called on each creature present in a fight
        //(excepting the player).
        public virtual void CombatAI(Player player, List<Character> creatures) {
            throw new Exception("Creature combat handler for " + Name + " has not been overriden!");
        }

        public virtual Character SelectTarget(List<Character> oppositeTeam) {
            Character target = null;
            Dictionary<Character, int> possibleTargets = null;

            foreach (Character cre in oppositeTeam) {
                //Check if not Defeated this turn.
                if (cre.HP > 0 && cre.Lust < cre.LustMax)
                {
                    int weight = 10;
                    possibleTargets.Add(cre, weight);
                }
            }
            if (possibleTargets.Count == 0) target = null;
            else if (possibleTargets.Count == 1) target = possibleTargets.Keys.First();
            else target = WeightedRandom.WeightedRand(possibleTargets);

            return target;
        }

        #endregion
        ///Descriptions
        public string HairLengthDescription() => face.HairLengthDescription(body.Height);
    }
}
