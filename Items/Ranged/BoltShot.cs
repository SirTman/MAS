﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS.Items;

namespace MAS.Items
{
    public class BoltShot : Item
    {
        public BoltShot() {
            this.Name = "DEP Z-110 - Boltshot";
            this.shortName = "Boltshot";

            this.Description = "Slug Thrower...";
            this.Type = ItemType.Ranged_Weapon;
            this.basePrice = 100.50m;

            //Stats
            this.Damage = 3;
            this.DamageType = DamageType.RADIANT;
            this.CritBonus = 1;

            this.Weight = WeightVerb.LIGHT;
            this.IsUsable = true;
            this.Ranged = true;
        }

        public override object Activate()
        {
            return "okay";
        }
    }
}
