﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS;

namespace MAS.Items
{
    public abstract class Item
    {
        public string Name { protected set;  get; }
        public string shortName { protected set; get; }
        public string Description { protected set; get; }

        public ItemType Type { protected set; get; }
        public bool isWeapon { get {
                if (Type == ItemType.Melee_Weapon ||
                    Type == ItemType.Ranged_Weapon ||
                    Type == ItemType.Explosive)
                    return true;
                else return false;
            } }
        public bool Ranged { get; protected set; }
        public decimal basePrice { protected set; get; }

        public List<Object> ItemProperties { protected set; get; }

        #region Stats
        //Weapons
        public DamageType DamageType { protected set; get;}
        public int Damage { protected set; get; }
        public int CritBonus { protected set; get; }

        public bool TwoHanded { get {
                if (ItemProperties.Contains(WepProperties.TWO_HANDED)) return true;
                else return false;
            }
        }

        //Armor
        public int Defense { protected set; get; }
        public int ShieldDefense { protected set; get; }
        public int Shields { protected set; get; }
        public int Sexiness { protected set; get; }
        
        //Misc
        public WeightVerb Weight { protected set; get; }
        public List<DamageType> Resistances { protected set; get; }
        public bool IsUsable { set;  get; }
        #endregion

        #region Item Functions
        public abstract object Activate();
        public void Activate(object sender, EventArgs e) => Activate();
        #endregion
    }
}
