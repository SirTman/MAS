﻿using MAS;
using MAS.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAS.Rooms.Scenes
{
    class Workshop : Room
    {
        public Workshop()
        {
            //this.MainForm = MAS.MainForm;
            this.RoomName = "Workshop";
            this.Output = RoomLong = "The workshop, the place you typically call home. These days from here you can go to a variety of places.";
            MainForm.AbLeft();

            MainForm.ClrButtons();
            //Define Buttons
            this.Buttonlist.Add(CreateButton("Combat Trial", CombatTester_Click));
            this.Buttonlist.Add(CreateButton("DamageTest", Debug_Click));
            this.Buttonlist.Add(CreateButton("TestFun!", Debug_Click2));
            //this.buttonlist.Add(addButton("Travel1", "Dawns Cafe", na_Click));
            //this.buttonlist.Add(addButton("Travel2", "Dance Club", na_Click));
        }

        private void NA_Click(object sender, EventArgs e)
        {
            MainForm.SetButtons(NextButton(Return_Click));
            MainForm.TextOutput = "Seems closed at the moment";
            //MainForm.scDialog(description);
        }

        private void CombatTester_Click(object sender, EventArgs e) 
        {
            Beruch bitch = new Beruch();
            MainForm.ClrButtons();
            bitch.FirstMeeting();
        }

        private void Debug_Click(object sender, EventArgs e) 
        {
            MainForm.PlayerCharacter.Damage(1);
            MainForm.UpdateHP();
        }
        private void Debug_Click2(object sender, EventArgs e)
        {
            PC.AddItem(new BoltShot());
            PC.EquipItem(new BoltShot());

            MainForm.TextOutput = PC.MainHand.Name;
            ReturnToStart();
            //Global.Credits += 1.50m;
        }
    }
}
