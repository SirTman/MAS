﻿using MAS;
using MAS.FormAdditions;
using MAS.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAS.Rooms.Scenes
{
    class CharacterCreation : Room
    {
        TextBox tmName;
        string playerName;
        bool isMale;
        Player temptPC;
        int pointbuy = 25;
        private int[] baseStatsTotals = { 8, 8, 8, 8, 8, 8 };
        private int[] statsPoints = { 0, 0, 0, 0, 0, 0 };
        private int[] racialBonus = { 0, 0, 0, 0, 0, 0 };

        bool DEBUG_SkipCreation = true;

        public CharacterCreation()
        {
            this.RoomName = "Registration Office";
            //this.DescriptionLong 
                this.Output = "You find yourself on the Planet of Capus B27 a wasteland ball of dust in a remote system of the galexy. " +
                "One of the only noticable things on this rock is Trash Town, a city build up of refuse and scrap discared from a warzone long ago" +
                "the town itself is rulleded by a cutthroat Queen. The biggest drawn of the metrolplise is the Karma Coliseum, one of the only ways" +
                "to get propper fame and recognistion around these parts. A bloody area that hosts Mech Battles, where winner takes all, in some weird sence.." +
                "\n\n" +
                "You for whatever reason have decided to join this bloodsport, you find yourself standing in a line nearly at the Admin desk";
            //this.MainForm = mAS;
            MainForm.AbRight();
            
            //DEBUG START
            if (DEBUG_SkipCreation)
            {
                playerName = "alex";
                isMale = false;
                this.temptPC = new Player(playerName, isMale);
                
                Global.SetStartingRace(StartingRace.Cow);
                racialBonus[0] += 4; //STR
                racialBonus[2] += 4; //CON
                racialBonus[5] -= 2; //INT
                temptPC.setSpecies(Global.PC_OriginalRace);

                temptPC.setThickness(50);//Thickness

                temptPC.CreateAbilityScores(
                    14 + racialBonus[0],
                    10 + racialBonus[1], 
                    14 + racialBonus[2],
                    10 + racialBonus[3],
                    10 + racialBonus[4],
                    10 + racialBonus[5]);



                MainForm.PlayerCharacter = this.temptPC;
                MainForm.UpdateStats();
                MainForm.PlayerCharacter.sleep();
                MainForm.UpdateHP();

                MainForm.PlayerCharacter.AddItem(new BoltShot());
                MainForm.PlayerCharacter.EquipItem(new BoltShot());

                Sc_assets();
            }
            //NORMAL START
            else
            {
                MainForm.SetButtons(NextButton(sc_name));
                //MainForm.setButtons(nextButton());
            }
        }
        
        //Name
        private void sc_name(object sender, EventArgs e) {
            MainForm.AbLeft();
            this.Output = "Next please? Ah, Another one to sign up, huh? They sure have been pushing that new compeditor slots after the last royale." +
                "Alright I'm just going to need you to fill out this form.... what's with that blank stare let's start simple;\nwhat is your name?";
            tmName = NameField(sc_gender);
            //pushDescription();
        }
        
        //Gender
        private void sc_gender(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tmName.Text))
            {
                playerName = tmName.Text;
                this.Output = string.Format("Ah, so your name is {0}, cute dear.\n" +
                            "So tell me {0} are you a Man or a Woman?", playerName);
                MainForm.SetButtons(CreateButton("Male", gender_Click));
                MainForm.AddButtons(CreateButton("Female", gender_Click, "Makes you female"));
                //pushDescription();
            }
        }

        private void gender_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            isMale = (button.Name == "Male") ?  true : false;
            this.temptPC = new Player(playerName, isMale);
            SC_race();
        }

        //Race
        private void SC_race()
        {
            this.Output = string.Format("Ah, so your a {0}, awesome.\n" +
                        "Next could you fill in your species?", this.temptPC.GetGender());
            Buttonlist.Clear();
            Buttonlist.Add(CreateButton("Human", Race_Click));
            Buttonlist.Add(CreateButton("Rabbit", Race_Click));
            Buttonlist.Add(CreateButton("Cat", Race_Click));
            Buttonlist.Add(CreateButton("Goat", Race_Click));
            Buttonlist.Add(CreateButton("Dog", Race_Click));
            Buttonlist.Add(CreateButton("Horse", Race_Click));
            Buttonlist.Add(CreateButton("Cow", Race_Click));
            PushActionBar();
            //pushDescription();
        }

        private void Race_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            switch (button.Name)
            {
                case "Human":
                    Global.SetStartingRace(StartingRace.Human);
                    pointbuy += 3;
                    break;
                case "Rabbit":
                    Global.SetStartingRace(StartingRace.Rabbit);
                    racialBonus[1] += 1; //RFX
                    racialBonus[4] += 2; //PRE
                    break;
                case "Cat":
                    Global.SetStartingRace(StartingRace.Cat);
                    racialBonus[0] += 1; //STR
                    racialBonus[1] += 2; //RFX
                    racialBonus[5] -= 1; //INT
                    break;
                case "Goat":
                    Global.SetStartingRace(StartingRace.Goat);
                    racialBonus[0] += 2; //STR
                    racialBonus[2] += 2; //CON
                    racialBonus[3] -= 1; //CUN
                    break;
                case "Dog":
                    Global.SetStartingRace(StartingRace.Dog);
                    racialBonus[0] += 2; //STR
                    racialBonus[5] += 1; //INT
                    racialBonus[3] += 1; //INT
                    break;
                case "Horse":
                    Global.SetStartingRace(StartingRace.Horse);
                    racialBonus[0] += 3; //STR
                    racialBonus[2] += 2; //CON
                    break;
                case "Cow":
                    Global.SetStartingRace(StartingRace.Cow);
                    racialBonus[0] += 4; //STR
                    racialBonus[2] += 4; //CON
                    racialBonus[5] -= 2; //INT
                    break;
            }
            temptPC.setSpecies(Global.PC_OriginalRace);
            SC_thickness();
        }

        //Thickness
        private void SC_thickness() {
            MainForm.PlayerCharacter = this.temptPC;
            Buttonlist.Clear();
            Buttonlist.Add(CreateButton("20", "Very Thin", Thickness_Click));
            Buttonlist.Add(CreateButton("30", "Thin", Thickness_Click));
            Buttonlist.Add(CreateButton("40", "Lithe", Thickness_Click));
            Buttonlist.Add(CreateButton("50", "Normal;", Thickness_Click));
            Buttonlist.Add(CreateButton("60", "Husky", Thickness_Click));
            Buttonlist.Add(CreateButton("70", "Thickset", Thickness_Click));
            PushActionBar();

            this.Output = string.Format("Oh, so you are a {1}, {0}.\n" +
                "As for your size class would you describe yourself as...?", temptPC.Name, temptPC.GetGender());
            //pushDescription();
        }

        private void Thickness_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            temptPC.setThickness(Int32.Parse(button.Name));
            SC_stats();
        }

        private void SC_stats() {
            Buttonlist.Clear();
            MainForm.ClrButtons();
            MainForm.AddButtons(StatUpButton("STR", StatUp, 0));
            MainForm.AddButtons(StatUpButton("RFX", StatUp, 1));
            MainForm.AddButtons(StatUpButton("CON", StatUp, 2));
            MainForm.AddButtons(StatUpButton("CUN", StatUp, 3));
            MainForm.AddButtons(StatUpButton("PRE", StatUp, 4));
            //MainForm.addButtons(statUpButton("INT", statUp, 5));
            Button buttonBreak = StatUpButton("INT", StatUp, 5);
            MainForm.AddButtons(buttonBreak);
            MainForm.ActionBarBreak(buttonBreak, true);

            MainForm.AddButtons(StatDownButton("STR", StatDown, 0));
            MainForm.AddButtons(StatDownButton("RFX", StatDown, 1));
            MainForm.AddButtons(StatDownButton("CON", StatDown, 2));
            MainForm.AddButtons(StatDownButton("CUN", StatDown, 3));
            MainForm.AddButtons(StatDownButton("PRE", StatDown, 4));
            MainForm.AddButtons(StatDownButton("INT", StatDown, 5));

            Button finishedButton = FinishSatsButton(SaveAbility);
            if (pointbuy >= 1)
                finishedButton.Enabled = false;
            MainForm.AddButtons(finishedButton);

            //MainForm.setButtons(buttonlist);
            this.Output = "Alright, next you are going to have to undergo an evaluation to test your stats." +
                "\n" +
                "Points to Spend: " + pointbuy + "\n\n" + StatCal();

            //pushDescription();
        }

        private string StatCal()
        {
            string strDialog = "";
            string[] str = {
                string.Format("STR TOTAL: {0}", baseStatsTotals[0] + statsPoints[0] + racialBonus[0]),
                string.Format("RFX TOTAL: {0}", baseStatsTotals[1] + statsPoints[1] + racialBonus[1]),
                string.Format("CON TOTAL: {0}", baseStatsTotals[2] + statsPoints[2] + racialBonus[2]),
                string.Format("CUN TOTAL: {0}", baseStatsTotals[3] + statsPoints[3] + racialBonus[3]),
                string.Format("PRE TOTAL: {0}", baseStatsTotals[4] + statsPoints[4] + racialBonus[4]),
                string.Format("INT TOTAL: {0}", baseStatsTotals[5] + statsPoints[5] + racialBonus[5])};

            for (int i = 0; i < str.Length; i++) {
                strDialog += str[i] + " + "+ statsPoints[i] + " (points)";

                switch (Global.PC_OriginalRace)
                {
                    case StartingRace.Rabbit:
                        if (i == 1) strDialog += " + 1 (Racial Bonus)"; //RFX
                        if (i == 4) strDialog += " + 2 (Racial Bonus)"; //PRE
                        break;
                    case StartingRace.Cat:
                        if (i == 0) strDialog += " + 1 (Racial Bonus)"; //STR
                        if (i == 1) strDialog += " + 2 (Racial Bonus)"; //RFX
                        if (i == 5) strDialog += " - 1 (Racial Bonus)"; //INT
                        break;
                    case StartingRace.Goat:
                        if (i == 0) strDialog += " + 2 (Racial Bonus)"; //STR
                        if (i == 2) strDialog += " + 2 (Racial Bonus)"; //CON
                        if (i == 3) strDialog += " - 1 (Racial Bonus)"; //CUN
                        break;
                    case StartingRace.Dog:
                        if (i == 0) strDialog += " + 2 (Racial Bonus)"; //STR
                        if (i == 5) strDialog += " + 1 (Racial Bonus)"; //INT
                        if (i == 3) strDialog += " + 1 (Racial Bonus)"; //INT
                        break;
                    case StartingRace.Horse:
                        if (i == 0) strDialog += " + 3 (Racial Bonus)"; //STR
                        if (i == 2) strDialog += " + 2 (Racial Bonus)"; //CON
                        break;
                    case StartingRace.Cow:
                        if (i == 0) strDialog += " + 4 (Racial Bonus)"; //STR
                        if (i == 2) strDialog += " + 4 (Racial Bonus)"; //CON
                        if (i == 5) strDialog += " - 2 (Racial Bonus)"; //INT
                        break;
                }
                strDialog += "\n";
            }
            return strDialog;
        }

        private void StatUp(object sender, EventArgs e) {
            var button = (Button)sender;
            int val = Int32.Parse(button.Name);
            switch (baseStatsTotals[val] + statsPoints[val])
            {
                case 6: // -> 7
                    if (pointbuy >= 3)
                    {
                        statsPoints[val] += 1;
                        pointbuy -= 3;
                    }
                    break;
                case 7: // -> 8
                    if (pointbuy >= 2)
                    {
                        statsPoints[val] += 1;
                        pointbuy -= 2;
                    }
                    break;
                case 8: // -> 9
                    if (pointbuy >= 1) { 
                        statsPoints[val] += 1;
                        pointbuy -= 1;
                    }
                    break;
                case 9:// -> 10
                    if (pointbuy >= 2) { 
                        statsPoints[val] += 1;
                        pointbuy -= 2;
                    }
                    break;
                case 10:// -> 11
                    if (pointbuy >= 3) { 
                        statsPoints[val] += 1;
                        pointbuy -= 3;
                    }
                    break;
                case 11:// -> 12
                    if (pointbuy >= 4) {
                        statsPoints[val] += 1;
                        pointbuy -= 4;
                    }
                    break;
                case 12:// -> 13
                    if (pointbuy >= 5) {
                        statsPoints[val] += 1;
                        pointbuy -= 5;
                    }
                    break;
                case 13:// -> 14
                    if (pointbuy >= 7) {
                        statsPoints[val] += 1;
                        pointbuy -= 7;
                    }
                    break;
                case 14:// -> 15
                    if (pointbuy >= 9) {
                        statsPoints[val] += 1;
                        pointbuy -= 9;
                    }
                    break;
            }
            SC_stats();
        }
        private void StatDown(object sender, EventArgs e)
        {
            var button = (Button)sender;
            int val = Int32.Parse(button.Name);
            switch (baseStatsTotals[val] + statsPoints[val])
            {
                case 7: // -> 6
                    statsPoints[val] -= 1;
                    pointbuy += 3;
                    break;
                case 8: // -> 7
                    statsPoints[val] -= 1;
                    pointbuy += 2;
                    break;
                case 9: // -> 8
                    statsPoints[val] -= 1;
                    pointbuy += 1;
                    break;
                case 10:// -> 9
                    statsPoints[val] -= 1;
                    pointbuy += 2;
                    break;
                case 11:// -> 10
                    statsPoints[val] -= 1;
                    pointbuy += 3;
                    break;
                case 12:// -> 11
                    statsPoints[val] -= 1;
                    pointbuy += 4;
                    break;
                case 13:// -> 12
                    statsPoints[val] -= 1;
                    pointbuy += 5;
                    break;
                case 14:// -> 13
                    statsPoints[val] -= 1;
                    pointbuy += 7;
                    break;
                case 15:// -> 14
                    statsPoints[val] -= 1;
                    pointbuy += 9;
                    break;
            }
            SC_stats();
        }

        private void SaveAbility(object sender, EventArgs e) {
            for (int i = 0; i < baseStatsTotals.Length; i++)
            {
                temptPC.SetAbilityScore((Stat)i, (baseStatsTotals[i] + statsPoints[i] + racialBonus[i]));
            }
            MainForm.PlayerCharacter = this.temptPC;
            MainForm.UpdateStats();
            MainForm.PlayerCharacter.sleep();
            MainForm.UpdateHP();
            Sc_assets();
        }

        private void Sc_assets() {
            this.Output = "Well now that is all done, it's time for you to head to the workshop";
            MainForm.SetButtons(NextButton(MainForm.ReturnToWorkshop));
            //pushDescription();
        }
    }
}
