﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MAS;
using MAS.FormAdditions;

//using player = MAS.

namespace MAS.Rooms
{
    public abstract class Room
    {
        protected MAS MainForm = MAS.MainForm;
        protected Player PC = MAS.MainForm.PlayerCharacter;

        public int ID { get; set; }
        public string RoomName { get; set; }
        public string RoomLong { get; protected set; }
        private string _output = null;
        public string Output { get { return _output; }
            set {
                _output = value;
                MainForm.TextOutput = value; } }
        //Constructors
        public Room() { }

        /*
        public Room(string Name, string desc, int setID)
        {
            this.RoomName = Name;
            this.DescriptionLong = desc;
            this.ID = setID;
            //this.MainForm = MAS.MainForm;
        }
        */
        //protected void pushDescription() => MainForm.TextOutput = this.TextOutput;

        #region Button Functions
        //List of Buttons
        public List<Button> Buttonlist { set; get; } = new List<Button>();
        //All Button Adding Commands
        public void PushActionBar() => MainForm.SetButtons(Buttonlist);
        public Button CreateButton(string bName, string bText, EventHandler eh) => ButtonFactory.AddButton(bName, bText, eh);
        public Button CreateButton(string NameTxt, EventHandler eh, string ToolTipText) => ButtonFactory.AddButton(NameTxt, eh, ToolTipText);
        public Button CreateButton(string NameTxt, EventHandler eh) => ButtonFactory.AddButton(NameTxt, eh);
        public Button StatUpButton(string NameTxt, EventHandler eh, int ablNum) => ButtonFactory.StatUpButton(NameTxt, eh, ablNum);
        public Button StatDownButton(string NameTxt, EventHandler eh, int ablNum) => ButtonFactory.StatDownButton(NameTxt, eh, ablNum);
        public Button FinishSatsButton(EventHandler eh) => ButtonFactory.FinishSatsButton(eh);
        public Button NextButton(EventHandler ne) => ButtonFactory.NextButton(ne);
        public Button BackButton(EventHandler pe) => ButtonFactory.BackButton(pe); 
        public TextBox NameField(EventHandler eh) => ButtonFactory.NameField(eh);

        //Main Refrences
        public void ClrButtons() { MainForm.ClrButtons(); }
        public void SetButtons(Button btn) => MainForm.SetButtons(btn);
        public void SetButtons(List<Button> btn) => MainForm.SetButtons(btn);
        public void AddButtons(Button btn) => MainForm.AddButtons(btn);
        public void AddButtons(List<Button> btn) => MainForm.AddButtons(btn);

        public void ReturnOption()
        {
            MainForm.AbAdd(NextButton(Return_Click));
        }
        public void ReturnToStart() {
            MainForm.SetButtons(NextButton(Return_Click));
        }
        protected void Return_Click(object sender, EventArgs e)
        {
            MainForm.SetButtons(Buttonlist);
            Output = RoomLong;
        }
        #endregion
    }
}
