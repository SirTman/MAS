﻿using MAS.Rooms;
using MAS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAS
{
    #region Enums
    public enum RacialType {
        HUMAN,
        CANINE,
        FELINE,
        EQUINE,
        BOVINE,
        LIZAN,
        DRACONIC,
        RABBIT,
        NAGA,
        RACCOON,
        VULPINE,
        LUPINE,
        MOUSEY,
        PANDA,
        REDPANDA,
        BADGER,
        KANGAROO,
        FROG,
        AVIAN,
        SHARK,
        ORCA,
        SWINE,
        SIMIAN,
        GOAT,
        ELVISH,
        GOOEY
    }
    public enum StartingRace
    {
        Human,
        Rabbit,
        Cat,
        Goat,
        Dog,
        Horse,
        Cow,
        //Custom
    }

    public enum Sex
    {
        male,
        female,
        andromorph,
        gynomorphy,
        herm,
        maleherm,
        undefined
    }

    public enum Stat
    {
        STR, RFX, CON, CUN, PRE, INT
    }

    public enum DamageType 
    {
        ACID,
        BLUDGEONING,
        COLD,
        FIRE,
        FORCE,
        LIGHTNING,
        LUST,
        NECROTIC,
        PIERCING,
        PSYCHIC,
        RADIANT,
        SLASHING,
        SONIC,
        TOXIC
    }
    
    public enum ItemType
    {
        Pill,
		Food,
		Potion,
		Melee_Weapon,
		Ranged_Weapon,
		Armor,
		Shield,
		Upper_Undergarment,
		Lower_Undergarment,
		Accessory,
		Drug,
		Clothing,
		All,
		Gadget,
		Explosive,
		Quest_Item,
		Gem,
		Sex_Toy,
		Piercing,
		Cockwear,
		Tent
    }

    public enum WepProperties {
        AMMUNITION,
        FINESSE,
        BIG,
        LIGHT,
        LOADING,
        RANGE,
        REACH,
        SPECIAL,
        THROWN,
        TWO_HANDED,
        VERSATILE,
        IMPROVISED
    }
    public enum WepTrait {
        ACCURATE,
        AUTO_FIRE,
        BLAST,
        BREACH,
        BURN,
        CONCUSSIVE,
        INDESTRUCTIBLE,
        LUST,
        SUNDER,
        VICIOUS
    }
    public enum WeightVerb {
        NOTHING, LIGHT, MEDIUM, HEAVY, SUPER_Heavy
    }
    #endregion
    public static class Global
    {
        //System Globals
        public const int bw = 100;
        public const int bh = 35;

        //Player Globals
        public static StartingRace PC_OriginalRace;
        public static void SetStartingRace(StartingRace type) => PC_OriginalRace = type;
        public static Sex PC_OrginalGender;

        private static decimal _credits;
        public static decimal Credits {
            set {
                _credits = value;
                MAS.MainForm.CreditLabel();
            }
            get { return _credits; } }
        public static double MASS { set; get; }
        
        //Game Globals
        public static Room CurrentRoom { set; get; }
        public static Room PreviousRoom { set; get; }
        //public static MAS MainForm { get { MAS.ActiveForm; } }
    }
}
