﻿using MAS.Rooms;
using MAS.Rooms.Scenes;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MAS
{
    public partial class MAS : Form
    {
        public static MAS MainForm { set; get; }
        private Room _scene;
        public Room Scene
        {
            get { return _scene; }
            set
            {
                Global.PreviousRoom = Scene;
                _scene = value;
                Global.CurrentRoom = value;
                CurLocation.Text = "LOCATION: " + Scene.RoomName;
                TextOutput = Scene.Output;

                //ClrButtons();
                for (int i = 0; i < Scene.Buttonlist.Count; i++)
                {
                    ActionBar.Controls.Add(Scene.Buttonlist[i]);
                }
            }
        }
        public string TextOutput { set { TBMainTextWin.Text = value; } } //Outputs to the Scene Description

        public void CreditLabel() {
            creditLabel.Text = $"CREDITS: ¤{Global.Credits}";
        }

        //Player
        private static Player _playerCharacter;
        public Player PlayerCharacter {
            set {
                labelName.Text = value.Name;
                _playerCharacter = value;
            }
            get { return _playerCharacter; }
        }
        public void UpdatePlayerNameLabel() => labelName.Text = PlayerCharacter.Name;

        public MAS()
        {
            InitializeComponent();
            MainForm = this;
        }
        #region Form Control Manipulation
        //==================================================================================

        /// <summary>Sets the Buttong Alignments to the left</summary>
        public void AbLeft()=>ActionBar.FlowDirection = FlowDirection.LeftToRight;
        
        /// <summary>Sets the Buttong Alignments to the Right</summary>
        public void AbRight()=>ActionBar.FlowDirection = FlowDirection.RightToLeft;
        
        /// <summary>Adds a Control to the Actionbar</summary>
        public void AbAdd(Control el)=>ActionBar.Controls.Add(el);
        
        /// <summary>Clears all buttons on the Actionbar</summary>
        public void ClrButtons() => ActionBar.Controls.Clear();
        
        /// <summary>Adds indervidual buttons on the Actionbar</summary>
        public void AddButtons(Button but) => ActionBar.Controls.Add(but);
        
        /// <summary>Clears the ActionBar then sets a button</summary>
        public void SetButtons(Button but) {
            ClrButtons();
            ActionBar.Controls.Add(but); 
        }
        
        /// <summary>Adds a list of Buttons to the ActionBar</summary>
        public void AddButtons(List<Button> but) {
            for (int i = 0; i < but.Count; i++)
            {
                ActionBar.Controls.Add(but[i]);
            }
        }
        
        /// <summary>Clears the ActionBar then adds a list of Buttons to the ActionBar</summary>
        public void SetButtons(List<Button> but) {
            ClrButtons();
            for (int i = 0; i < but.Count; i++)
            {
                ActionBar.Controls.Add(but[i]);
            }
        }

        /// <summary>Forces buttons to the next row</summary>
        public void ActionBarBreak(Control control, bool value) => ActionBar.SetFlowBreak(control, value);

        /// <summary>Add Foe Box</summary>
        public void AddFoeBox(GroupBox box) => foeLayoutPanel.Controls.Add(box);
        public bool RemoveFoeBox(Character foe) {
            foreach (GroupBox foeBox in foeLayoutPanel.Controls) {
                if (foeBox.Tag == foe) {
                    foeLayoutPanel.Controls.Remove(foeBox);
                    foeBox.Dispose();
                    return true;
                }
            }
            return false;
        }
        public void ClearFoeBoxs() {
            foeLayoutPanel.Controls.Clear();
        }

        public void UpdateStats()
        {
            StrScore.Text = PlayerCharacter.GetAbilityScore(Stat.STR).ToString();
            RfxScore.Text = PlayerCharacter.GetAbilityScore(Stat.RFX).ToString();
            ConScore.Text = PlayerCharacter.GetAbilityScore(Stat.CON).ToString();
            CunScore.Text = PlayerCharacter.GetAbilityScore(Stat.CUN).ToString();
            PreScore.Text = PlayerCharacter.GetAbilityScore(Stat.PRE).ToString();
            IntScore.Text = PlayerCharacter.GetAbilityScore(Stat.INT).ToString();
        }
        public void UpdateHP() {
            try {
                labelHPMax.Text = PlayerCharacter.HPmax.ToString();
                labelHPCurrent.Text = PlayerCharacter.HP.ToString();
                hitPointsBar.Maximum = PlayerCharacter.HPmax;
                hitPointsBar.Value = PlayerCharacter.HP;
            }
            catch (ArgumentOutOfRangeException) {
                if (PlayerCharacter.HP < 0)
                    hitPointsBar.Value = 0;
                else if (PlayerCharacter.HP > hitPointsBar.Maximum)
                    hitPointsBar.Value = hitPointsBar.Maximum;
            }

            //hitPointsBar.PerformStep();
        }
        
        public void ReturnToWorkshop(object sender, EventArgs e)
        {
            Scene = new Workshop();
            ClearFoeBoxs();
        }

        private void MAS_Load(object sender, EventArgs e)
        {
            //SetScene(Scene);
            Scene = new CharacterCreation();
        }
        private void StrScore_TextChanged(object sender, EventArgs e)
        {
            string format = "+0;-#;";
            strMod.Text = "(" + PlayerCharacter.STR.ToString(format) + ")";
        }
        private void RfxScore_TextChanged(object sender, EventArgs e)
        {
            string format = "+0;-#;";
            rfxMod.Text = "(" + PlayerCharacter.RFX.ToString(format) + ")";
        }
        private void ConScore_TextChanged(object sender, EventArgs e)
        {
            string format = "+0;-#;";
            conMod.Text = "(" + PlayerCharacter.CON.ToString(format) + ")";
        }
        private void CunScore_TextChanged(object sender, EventArgs e)
        {
            string format = "+0;-#;";
            cunMod.Text = "(" + PlayerCharacter.CUN.ToString(format) + ")";
        }
        private void PreScore_TextChanged(object sender, EventArgs e)
        {
            string format = "+0;-#;";
            preMod.Text = "(" + PlayerCharacter.PRE.ToString(format) + ")";
        }
        private void IntScore_TextChanged(object sender, EventArgs e)
        {
            string format = "+0;-#;";
            intMod.Text = "(" + PlayerCharacter.INT.ToString(format) + ")";
        }
        #endregion
    }
}
