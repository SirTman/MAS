﻿namespace MAS
{
    partial class MAS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TBMainTextWin = new System.Windows.Forms.RichTextBox();
            this.CurLocation = new System.Windows.Forms.Label();
            this.statBox = new System.Windows.Forms.GroupBox();
            this.creditLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelHPMax = new System.Windows.Forms.Label();
            this.labelHPCurrent = new System.Windows.Forms.Label();
            this.labelhpDiv = new System.Windows.Forms.Label();
            this.hitPointsBar = new System.Windows.Forms.ProgressBar();
            this.labelHP = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.intMod = new System.Windows.Forms.Label();
            this.preMod = new System.Windows.Forms.Label();
            this.cunMod = new System.Windows.Forms.Label();
            this.conMod = new System.Windows.Forms.Label();
            this.rfxMod = new System.Windows.Forms.Label();
            this.IntScore = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PreScore = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CunScore = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ConScore = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.RfxScore = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.StrScore = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.strMod = new System.Windows.Forms.Label();
            this.ActionBar = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.foeGroupBoxT = new System.Windows.Forms.GroupBox();
            this.hpTemp = new System.Windows.Forms.Label();
            this.foeHPBar = new System.Windows.Forms.ProgressBar();
            this.foeLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.labelName = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.statBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.ActionBar.SuspendLayout();
            this.foeGroupBoxT.SuspendLayout();
            this.foeLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TBMainTextWin
            // 
            this.TBMainTextWin.BackColor = System.Drawing.Color.Silver;
            this.TBMainTextWin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TBMainTextWin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TBMainTextWin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TBMainTextWin.Font = new System.Drawing.Font("Lucida Console", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBMainTextWin.HideSelection = false;
            this.TBMainTextWin.Location = new System.Drawing.Point(10, 10);
            this.TBMainTextWin.Margin = new System.Windows.Forms.Padding(2);
            this.TBMainTextWin.Name = "TBMainTextWin";
            this.TBMainTextWin.ReadOnly = true;
            this.TBMainTextWin.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.TBMainTextWin.Size = new System.Drawing.Size(598, 398);
            this.TBMainTextWin.TabIndex = 1;
            this.TBMainTextWin.Text = "Test";
            // 
            // CurLocation
            // 
            this.CurLocation.AutoSize = true;
            this.CurLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurLocation.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.CurLocation.Location = new System.Drawing.Point(148, 7);
            this.CurLocation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CurLocation.Name = "CurLocation";
            this.CurLocation.Size = new System.Drawing.Size(137, 20);
            this.CurLocation.TabIndex = 3;
            this.CurLocation.Text = "LOCATION: ???";
            // 
            // statBox
            // 
            this.statBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(99)))), ((int)(((byte)(155)))));
            this.statBox.Controls.Add(this.creditLabel);
            this.statBox.Controls.Add(this.tableLayoutPanel2);
            this.statBox.Controls.Add(this.hitPointsBar);
            this.statBox.Controls.Add(this.labelHP);
            this.statBox.Controls.Add(this.tableLayoutPanel1);
            this.statBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statBox.Location = new System.Drawing.Point(9, 58);
            this.statBox.Margin = new System.Windows.Forms.Padding(2);
            this.statBox.Name = "statBox";
            this.statBox.Padding = new System.Windows.Forms.Padding(2);
            this.statBox.Size = new System.Drawing.Size(134, 543);
            this.statBox.TabIndex = 4;
            this.statBox.TabStop = false;
            this.statBox.Text = "Stats";
            // 
            // creditLabel
            // 
            this.creditLabel.AutoSize = true;
            this.creditLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditLabel.Location = new System.Drawing.Point(1, 390);
            this.creditLabel.Name = "creditLabel";
            this.creditLabel.Size = new System.Drawing.Size(96, 17);
            this.creditLabel.TabIndex = 7;
            this.creditLabel.Text = "Credits: ¤0.00";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.labelHPMax, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelHPCurrent, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelhpDiv, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(32, 328);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(85, 24);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // labelHPMax
            // 
            this.labelHPMax.AutoSize = true;
            this.labelHPMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPMax.Location = new System.Drawing.Point(67, 0);
            this.labelHPMax.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelHPMax.Name = "labelHPMax";
            this.labelHPMax.Size = new System.Drawing.Size(16, 17);
            this.labelHPMax.TabIndex = 14;
            this.labelHPMax.Text = "0";
            this.labelHPMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelHPCurrent
            // 
            this.labelHPCurrent.AutoSize = true;
            this.labelHPCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPCurrent.Location = new System.Drawing.Point(32, 0);
            this.labelHPCurrent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelHPCurrent.Name = "labelHPCurrent";
            this.labelHPCurrent.Size = new System.Drawing.Size(16, 17);
            this.labelHPCurrent.TabIndex = 15;
            this.labelHPCurrent.Text = "0";
            this.labelHPCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelhpDiv
            // 
            this.labelhpDiv.AutoSize = true;
            this.labelhpDiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelhpDiv.Location = new System.Drawing.Point(52, 0);
            this.labelhpDiv.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelhpDiv.Name = "labelhpDiv";
            this.labelhpDiv.Size = new System.Drawing.Size(11, 17);
            this.labelhpDiv.TabIndex = 13;
            this.labelhpDiv.Text = "/";
            this.labelhpDiv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hitPointsBar
            // 
            this.hitPointsBar.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.hitPointsBar.Location = new System.Drawing.Point(10, 353);
            this.hitPointsBar.Margin = new System.Windows.Forms.Padding(2);
            this.hitPointsBar.MarqueeAnimationSpeed = 10;
            this.hitPointsBar.Maximum = 0;
            this.hitPointsBar.Name = "hitPointsBar";
            this.hitPointsBar.Size = new System.Drawing.Size(116, 19);
            this.hitPointsBar.Step = 1;
            this.hitPointsBar.TabIndex = 1;
            // 
            // labelHP
            // 
            this.labelHP.AutoSize = true;
            this.labelHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.labelHP.Location = new System.Drawing.Point(7, 328);
            this.labelHP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelHP.Name = "labelHP";
            this.labelHP.Size = new System.Drawing.Size(27, 17);
            this.labelHP.TabIndex = 0;
            this.labelHP.Text = "HP";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.intMod, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.preMod, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cunMod, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.conMod, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.rfxMod, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.IntScore, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.PreScore, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.CunScore, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ConScore, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.RfxScore, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.StrScore, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.strMod, 2, 0);
            this.tableLayoutPanel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 33);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(113, 292);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // intMod
            // 
            this.intMod.AutoSize = true;
            this.intMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intMod.Location = new System.Drawing.Point(79, 239);
            this.intMod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.intMod.Name = "intMod";
            this.intMod.Size = new System.Drawing.Size(24, 17);
            this.intMod.TabIndex = 16;
            this.intMod.Text = "??";
            this.intMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // preMod
            // 
            this.preMod.AutoSize = true;
            this.preMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.preMod.Location = new System.Drawing.Point(79, 191);
            this.preMod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.preMod.Name = "preMod";
            this.preMod.Size = new System.Drawing.Size(24, 17);
            this.preMod.TabIndex = 15;
            this.preMod.Text = "??";
            this.preMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cunMod
            // 
            this.cunMod.AutoSize = true;
            this.cunMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cunMod.Location = new System.Drawing.Point(79, 143);
            this.cunMod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cunMod.Name = "cunMod";
            this.cunMod.Size = new System.Drawing.Size(24, 17);
            this.cunMod.TabIndex = 14;
            this.cunMod.Text = "??";
            this.cunMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // conMod
            // 
            this.conMod.AutoSize = true;
            this.conMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conMod.Location = new System.Drawing.Point(79, 96);
            this.conMod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.conMod.Name = "conMod";
            this.conMod.Size = new System.Drawing.Size(24, 17);
            this.conMod.TabIndex = 13;
            this.conMod.Text = "??";
            this.conMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rfxMod
            // 
            this.rfxMod.AutoSize = true;
            this.rfxMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rfxMod.Location = new System.Drawing.Point(79, 48);
            this.rfxMod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.rfxMod.Name = "rfxMod";
            this.rfxMod.Size = new System.Drawing.Size(24, 17);
            this.rfxMod.TabIndex = 12;
            this.rfxMod.Text = "??";
            this.rfxMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IntScore
            // 
            this.IntScore.AutoSize = true;
            this.IntScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IntScore.Location = new System.Drawing.Point(51, 239);
            this.IntScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IntScore.Name = "IntScore";
            this.IntScore.Size = new System.Drawing.Size(24, 17);
            this.IntScore.TabIndex = 11;
            this.IntScore.Text = "??";
            this.IntScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.IntScore.TextChanged += new System.EventHandler(this.IntScore_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 239);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "INT";
            // 
            // PreScore
            // 
            this.PreScore.AutoSize = true;
            this.PreScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreScore.Location = new System.Drawing.Point(51, 191);
            this.PreScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PreScore.Name = "PreScore";
            this.PreScore.Size = new System.Drawing.Size(24, 17);
            this.PreScore.TabIndex = 10;
            this.PreScore.Text = "??";
            this.PreScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PreScore.TextChanged += new System.EventHandler(this.PreScore_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 191);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "PRE";
            // 
            // CunScore
            // 
            this.CunScore.AutoSize = true;
            this.CunScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CunScore.Location = new System.Drawing.Point(51, 143);
            this.CunScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CunScore.Name = "CunScore";
            this.CunScore.Size = new System.Drawing.Size(24, 17);
            this.CunScore.TabIndex = 9;
            this.CunScore.Text = "??";
            this.CunScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CunScore.TextChanged += new System.EventHandler(this.CunScore_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 143);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "CUN";
            // 
            // ConScore
            // 
            this.ConScore.AutoSize = true;
            this.ConScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConScore.Location = new System.Drawing.Point(51, 96);
            this.ConScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ConScore.Name = "ConScore";
            this.ConScore.Size = new System.Drawing.Size(24, 17);
            this.ConScore.TabIndex = 8;
            this.ConScore.Text = "??";
            this.ConScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ConScore.TextChanged += new System.EventHandler(this.ConScore_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 96);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "CON";
            // 
            // RfxScore
            // 
            this.RfxScore.AutoSize = true;
            this.RfxScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RfxScore.Location = new System.Drawing.Point(51, 48);
            this.RfxScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.RfxScore.Name = "RfxScore";
            this.RfxScore.Size = new System.Drawing.Size(24, 17);
            this.RfxScore.TabIndex = 7;
            this.RfxScore.Text = "??";
            this.RfxScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RfxScore.TextChanged += new System.EventHandler(this.RfxScore_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "RFX";
            // 
            // StrScore
            // 
            this.StrScore.AutoSize = true;
            this.StrScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StrScore.Location = new System.Drawing.Point(51, 0);
            this.StrScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.StrScore.Name = "StrScore";
            this.StrScore.Size = new System.Drawing.Size(24, 17);
            this.StrScore.TabIndex = 6;
            this.StrScore.Text = "??";
            this.StrScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.StrScore.TextChanged += new System.EventHandler(this.StrScore_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "STR";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // strMod
            // 
            this.strMod.AutoSize = true;
            this.strMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.strMod.Location = new System.Drawing.Point(79, 0);
            this.strMod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.strMod.Name = "strMod";
            this.strMod.Size = new System.Drawing.Size(24, 17);
            this.strMod.TabIndex = 7;
            this.strMod.Text = "??";
            this.strMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ActionBar
            // 
            this.ActionBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(78)))), ((int)(((byte)(120)))));
            this.ActionBar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ActionBar.Controls.Add(this.button1);
            this.ActionBar.Controls.Add(this.button2);
            this.ActionBar.Controls.Add(this.button3);
            this.ActionBar.Controls.Add(this.button4);
            this.ActionBar.Controls.Add(this.button5);
            this.ActionBar.Controls.Add(this.button6);
            this.ActionBar.Controls.Add(this.button7);
            this.ActionBar.Controls.Add(this.button8);
            this.ActionBar.Controls.Add(this.button9);
            this.ActionBar.Controls.Add(this.button10);
            this.ActionBar.Controls.Add(this.button11);
            this.ActionBar.Controls.Add(this.button12);
            this.ActionBar.Controls.Add(this.button13);
            this.ActionBar.Controls.Add(this.button14);
            this.ActionBar.ForeColor = System.Drawing.Color.Black;
            this.ActionBar.Location = new System.Drawing.Point(147, 457);
            this.ActionBar.Margin = new System.Windows.Forms.Padding(2);
            this.ActionBar.Name = "ActionBar";
            this.ActionBar.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.ActionBar.Size = new System.Drawing.Size(619, 143);
            this.ActionBar.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(2, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.AutoSize = true;
            this.button2.Location = new System.Drawing.Point(89, 10);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 41);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.Location = new System.Drawing.Point(176, 10);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 41);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.Location = new System.Drawing.Point(263, 10);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(83, 41);
            this.button4.TabIndex = 3;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.AutoSize = true;
            this.button5.Location = new System.Drawing.Point(350, 10);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 41);
            this.button5.TabIndex = 4;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.AutoSize = true;
            this.button6.Location = new System.Drawing.Point(437, 10);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(83, 41);
            this.button6.TabIndex = 5;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.AutoSize = true;
            this.button7.Location = new System.Drawing.Point(524, 10);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 41);
            this.button7.TabIndex = 6;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.AutoSize = true;
            this.button8.Location = new System.Drawing.Point(2, 55);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(83, 41);
            this.button8.TabIndex = 7;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.AutoSize = true;
            this.button9.Location = new System.Drawing.Point(89, 55);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(83, 41);
            this.button9.TabIndex = 8;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.AutoSize = true;
            this.button10.Location = new System.Drawing.Point(176, 55);
            this.button10.Margin = new System.Windows.Forms.Padding(2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(83, 41);
            this.button10.TabIndex = 9;
            this.button10.Text = "button10";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.AutoSize = true;
            this.button11.Location = new System.Drawing.Point(263, 55);
            this.button11.Margin = new System.Windows.Forms.Padding(2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(83, 41);
            this.button11.TabIndex = 10;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.AutoSize = true;
            this.button12.Location = new System.Drawing.Point(350, 55);
            this.button12.Margin = new System.Windows.Forms.Padding(2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(83, 41);
            this.button12.TabIndex = 11;
            this.button12.Text = "button12";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.AutoSize = true;
            this.button13.Location = new System.Drawing.Point(437, 55);
            this.button13.Margin = new System.Windows.Forms.Padding(2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(83, 41);
            this.button13.TabIndex = 12;
            this.button13.Text = "button13";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.AutoSize = true;
            this.button14.Location = new System.Drawing.Point(524, 55);
            this.button14.Margin = new System.Windows.Forms.Padding(2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(83, 41);
            this.button14.TabIndex = 13;
            this.button14.Text = "button14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // foeGroupBoxT
            // 
            this.foeGroupBoxT.Controls.Add(this.hpTemp);
            this.foeGroupBoxT.Controls.Add(this.foeHPBar);
            this.foeGroupBoxT.Location = new System.Drawing.Point(2, 2);
            this.foeGroupBoxT.Margin = new System.Windows.Forms.Padding(2);
            this.foeGroupBoxT.Name = "foeGroupBoxT";
            this.foeGroupBoxT.Padding = new System.Windows.Forms.Padding(2);
            this.foeGroupBoxT.Size = new System.Drawing.Size(166, 77);
            this.foeGroupBoxT.TabIndex = 6;
            this.foeGroupBoxT.TabStop = false;
            this.foeGroupBoxT.Text = "FoeName";
            // 
            // hpTemp
            // 
            this.hpTemp.AutoSize = true;
            this.hpTemp.Location = new System.Drawing.Point(5, 20);
            this.hpTemp.Name = "hpTemp";
            this.hpTemp.Size = new System.Drawing.Size(22, 13);
            this.hpTemp.TabIndex = 1;
            this.hpTemp.Text = "HP";
            // 
            // foeHPBar
            // 
            this.foeHPBar.ForeColor = System.Drawing.Color.GreenYellow;
            this.foeHPBar.Location = new System.Drawing.Point(33, 17);
            this.foeHPBar.Margin = new System.Windows.Forms.Padding(2);
            this.foeHPBar.MarqueeAnimationSpeed = 10;
            this.foeHPBar.Maximum = 0;
            this.foeHPBar.Name = "foeHPBar";
            this.foeHPBar.Size = new System.Drawing.Size(116, 19);
            this.foeHPBar.Step = 1;
            this.foeHPBar.TabIndex = 7;
            // 
            // foeLayoutPanel
            // 
            this.foeLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(99)))), ((int)(((byte)(155)))));
            this.foeLayoutPanel.Controls.Add(this.foeGroupBoxT);
            this.foeLayoutPanel.Location = new System.Drawing.Point(769, 36);
            this.foeLayoutPanel.Margin = new System.Windows.Forms.Padding(2);
            this.foeLayoutPanel.Name = "foeLayoutPanel";
            this.foeLayoutPanel.Size = new System.Drawing.Size(169, 565);
            this.foeLayoutPanel.TabIndex = 7;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelName.Location = new System.Drawing.Point(9, 36);
            this.labelName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(39, 20);
            this.labelName.TabIndex = 8;
            this.labelName.Text = "???";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.TBMainTextWin);
            this.panel1.Location = new System.Drawing.Point(146, 36);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(618, 418);
            this.panel1.TabIndex = 9;
            // 
            // MAS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(63)))), ((int)(((byte)(95)))));
            this.ClientSize = new System.Drawing.Size(946, 610);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.foeLayoutPanel);
            this.Controls.Add(this.ActionBar);
            this.Controls.Add(this.statBox);
            this.Controls.Add(this.CurLocation);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MAS";
            this.Text = "MAS";
            this.Load += new System.EventHandler(this.MAS_Load);
            this.statBox.ResumeLayout(false);
            this.statBox.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ActionBar.ResumeLayout(false);
            this.ActionBar.PerformLayout();
            this.foeGroupBoxT.ResumeLayout(false);
            this.foeGroupBoxT.PerformLayout();
            this.foeLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.RichTextBox TBMainTextWin;
        private System.Windows.Forms.Label CurLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label IntScore;
        public System.Windows.Forms.Label PreScore;
        public System.Windows.Forms.Label CunScore;
        public System.Windows.Forms.Label ConScore;
        public System.Windows.Forms.Label RfxScore;
        public System.Windows.Forms.Label StrScore;
        private System.Windows.Forms.FlowLayoutPanel ActionBar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        public System.Windows.Forms.GroupBox statBox;
        private System.Windows.Forms.Label labelHP;
        private System.Windows.Forms.ProgressBar hitPointsBar;
        public System.Windows.Forms.Label strMod;
        public System.Windows.Forms.Label intMod;
        public System.Windows.Forms.Label preMod;
        public System.Windows.Forms.Label cunMod;
        public System.Windows.Forms.Label conMod;
        public System.Windows.Forms.Label rfxMod;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Label labelhpDiv;
        public System.Windows.Forms.Label labelHPCurrent;
        public System.Windows.Forms.Label labelHPMax;
        private System.Windows.Forms.GroupBox foeGroupBoxT;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Label hpTemp;
        public System.Windows.Forms.FlowLayoutPanel foeLayoutPanel;
        private System.Windows.Forms.ProgressBar foeHPBar;
        public System.Windows.Forms.Label creditLabel;
        private System.Windows.Forms.Panel panel1;
    }
}

