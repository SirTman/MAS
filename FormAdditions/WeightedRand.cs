﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAS.FormAdditions
{
    class WeightedRandom
    {
        public static Character WeightedRand(Dictionary<Character,int> args) {
            Character option = null;
            var rand = new Random();

            int total = 0;
            foreach (int w in args.Values) {
                total += w;
            }
            int randSelection = rand.Next(total) + 1;

            foreach (KeyValuePair<Character, int> i in args) {
                randSelection -= i.Value;
                if (randSelection <= 0) option = i.Key;
            }
            return option;
        }
    }
}
