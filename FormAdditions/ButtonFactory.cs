﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MAS;

namespace MAS.FormAdditions
{
    public abstract class ButtonFactory
    {
        #region Button Functions
        public static Button AddButton(string bName, string bText, EventHandler eh)
        {
            Button newButton = new Button();
            //newButton.Font = new System.Drawing.Font("PF Ronda Seven", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            newButton.Size = new Size(Global.bw, Global.bh);
            newButton.TabIndex = 7;
            newButton.Name = bName;
            newButton.Text = bText;
            newButton.UseVisualStyleBackColor = true;
            newButton.Click += eh;
            return newButton;
        }
        public static Button AddButton(string NameTxt, EventHandler eh, string ToolTipText)
        {
            Button newButton = new Button();
            //newButton.Font = new System.Drawing.Font("PF Ronda Seven", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            newButton.Size = new Size(Global.bw, Global.bh);
            newButton.TabIndex = 7;
            newButton.Name = NameTxt;
            newButton.Text = NameTxt;
            newButton.UseVisualStyleBackColor = true;
            newButton.Click += eh;

            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(newButton, ToolTipText);

            return newButton;
        }
        public static Button AddButton(string NameTxt, EventHandler eh)
        {
            Button newButton = new Button();
            //newButton.Font = new System.Drawing.Font("PF Ronda Seven", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            newButton.Size = new Size(Global.bw, Global.bh);
            newButton.TabIndex = 7;
            newButton.Name = NameTxt;
            newButton.Text = NameTxt;
            newButton.UseVisualStyleBackColor = true;
            newButton.Click += eh;

            return newButton;
        }

        public static Button StatUpButton(string NameTxt, EventHandler eh, int ablNum)
        {
            Button newButton = new Button();
            newButton.Size = new Size(Global.bw, Global.bh / 2);
            newButton.TabIndex = 7;
            newButton.Name = ablNum.ToString();
            newButton.Text = "(" + NameTxt + ") +";
            newButton.UseVisualStyleBackColor = true;
            newButton.Click += eh;

            return newButton;
        }
        public static Button StatDownButton(string NameTxt, EventHandler eh, int ablNum)
        {
            Button newButton = new Button();
            newButton.Size = new Size(Global.bw, Global.bh / 2);
            newButton.TabIndex = 7;
            newButton.Name = ablNum.ToString();
            newButton.Text = "(" + NameTxt + ") -";
            newButton.UseVisualStyleBackColor = true;
            newButton.Click += eh;

            return newButton;
        }

        public static Button FinishSatsButton(EventHandler eh)
        {
            Button newButton = new Button();
            newButton.Size = new Size(Global.bw, Global.bh / 2);
            newButton.TabIndex = 7;
            newButton.Text = "Finished";
            newButton.UseVisualStyleBackColor = true;
            newButton.Click += eh;

            return newButton;
        }

        public static Button NextButton(EventHandler ne)
        {
            Button NextButton = AddButton("btNext", "   Next >>", ne);
            return NextButton;
        }

        public static Button BackButton(EventHandler pe)
        {
            Button BackButton = AddButton("btback", "<< Back   ", pe);
            return BackButton;
        }

        public static void ClrButtons() => MAS.MainForm.ClrButtons();
        //Function Return
        public static TextBox NameField(EventHandler eh)
        {
            ClrButtons();

            TextBox NameInput = new TextBox()
            {
                Name = "NameInput",
                TabIndex = 9,
                Size = new Size(500, Global.bh),
                Font = new Font("Microsoft Sans Serif", 22.2F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)))
            };

            MAS.MainForm.AbAdd(NameInput);
            Button newButton = new Button() { Size = new Size(Global.bw, Global.bh), TabIndex = 7, Name = "OK", Text = "Ok", UseVisualStyleBackColor = true };
            newButton.Click += new EventHandler(eh);
            MAS.MainForm.AbAdd(newButton);

            return NameInput;
        }
        #endregion
    }
}
