﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAS.FormAdditions
{
    class BattleFactory
    {
        public static GroupBox FoeBox(ref Character foe) {
            GroupBox foeBox = new GroupBox();
            foeBox.Location = new System.Drawing.Point(2, 2);
            foeBox.Margin = new System.Windows.Forms.Padding(2);
            foeBox.Name = "foeGroupBoxT";
            foeBox.Padding = new System.Windows.Forms.Padding(2);
            foeBox.Size = new System.Drawing.Size(166, 77);
            foeBox.TabIndex = 6;
            foeBox.TabStop = false;
            foeBox.Text = foe.Name;
            foeBox.Tag = foe;

            foeBox.Controls.Add(HPLabelMaker());
            foeBox.Controls.Add(HPBarMaker(foe.HPmax));


            return foeBox;
        }

        private static Label HPLabelMaker()
        {
            Label nl = new Label();
            nl.AutoSize = true;
            nl.Location = new System.Drawing.Point(5, 20);
            nl.Name = "hpTemp";
            nl.Size = new System.Drawing.Size(22, 13);
            nl.TabIndex = 1;
            nl.Text = "HP";
            return nl;
        }

        private static ProgressBar HPBarMaker(int hpMax)
        {
            ProgressBar hp = new ProgressBar();
            hp.ForeColor = System.Drawing.Color.GreenYellow;
            hp.Location = new System.Drawing.Point(33, 17);
            hp.Margin = new System.Windows.Forms.Padding(2);
            hp.MarqueeAnimationSpeed = 10;
            hp.Minimum = 0;
            hp.Maximum = hpMax;
            hp.Value = hpMax;
            hp.Name = "foeHPBar";
            hp.Size = new System.Drawing.Size(116, 19);
            hp.Step = 1;
            hp.TabIndex = 2;
            return hp;
        }
    }
}
