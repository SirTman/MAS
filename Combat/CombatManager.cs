﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MAS;
using MAS.Actions;
using MAS.FormAdditions;
using MAS.Rooms;

namespace MAS
{
    public class Combat
    {
        private Room Scene { set; get; } = Global.CurrentRoom;
        private MAS MainForm { set; get; }
        private Player PC { set; get; }
        private List<Character> FoesList { set; get; } = new List<Character>();
        private bool inCombat { set; get; }
        private bool MeckFight { set; get; }
        public string CombatLongOverride { set; get; }

        #region Constructors
        public Combat()
        {
            Intalizer();
            ClearFoes();
        }
        public Combat(bool mechBattle)
        {
            Intalizer();
            this.MeckFight = mechBattle;
            ClearFoes();
        }
        public Combat(Character foe)
        {
            Intalizer();
            ClearFoes();

            AddFoe(ref foe);
        }
        public Combat(Character foe, bool mechBattle)
        {
            Intalizer();
            this.MeckFight = mechBattle;
            ClearFoes();

            AddFoe(ref foe);
        }

        private void Intalizer() {
            this.Scene = Global.CurrentRoom;
            this.MainForm = MAS.MainForm;
            this.PC = MAS.MainForm.PlayerCharacter;
        }
        #endregion
        private void CombatLong(object sender, EventArgs e) => CombatLong();
        private void CombatLong() {
        if (CombatLongOverride != null)
            Scene.Output = CombatLongOverride;
        else
            Scene.Output = "Desc Not Implimented yet";
        }


        public void AddFoe(ref Character foe) {
            FoesList.Add(foe);
            //Some UI Stuff
            MainForm.AddFoeBox(BattleFactory.FoeBox(ref foe));
        }
        public void RemoveFoe(Character foe) {
            FoesList.Remove(foe);
            MainForm.RemoveFoeBox(foe);
        }
        public void RemoveFoeAt(int index) {
            FoesList.RemoveAt(index);
            //Ui Stuff
        }
        public void ClearFoes() {
            FoesList.Clear();
            MainForm.ClearFoeBoxs();
        }

        
        private void CombatMenu() {
            List<Button> combatMenu = new List<Button>{
                Scene.CreateButton("Action", ActionList),
                Scene.CreateButton("Special", SpecialList),
                Scene.CreateButton("Inventory", null),
                Scene.CreateButton("Description", CombatLong),
                Scene.CreateButton("Run", MainForm.ReturnToWorkshop)
            };
            Scene.SetButtons(combatMenu);
        }
        private void CombatMenu(object sender, EventArgs e) => CombatMenu();
        
        #region Actions
        public void ActionList(object sender, EventArgs e) {
            List<Button> actionMenu = new List<Button>();
            if (PC.MainHand.Ranged) actionMenu.Add(Scene.CreateButton("Shoot", WeaponAttack));
            if (!PC.MainHand.Ranged) actionMenu.Add(Scene.CreateButton("Melee", WeaponAttack));


            actionMenu.Add(Scene.CreateButton("Back", CombatMenu));
            Scene.SetButtons(actionMenu);
        }
        public void WeaponAttack(object sender, EventArgs e) {
            WeaponAttack atk = new WeaponAttack(PC.MainHand, PC);
            if (FoesList.First().Hit(atk))
                Scene.Output = $"({atk.ToHit}) You hit {FoesList.First().Name}, for {atk.DmgHeal} {atk.DmgType} damage.";
            else Scene.Output = $"({atk.ToHit}) You missed {FoesList.First().Name}";
            
            
            //TEMP

            Scene.SetButtons(Scene.NextButton(PlayerEndTurn));
        }



        #endregion

        #region Specials
        public void SpecialList(object sender, EventArgs e)
        {
            List<Button> specialMenu = new List<Button>{
                Scene.CreateButton("Back", CombatMenu)
            };
            Scene.SetButtons(specialMenu);
        }
        #endregion

        #region Foe Actions
        public void foeTurn() {
            foreach (Character foe in FoesList) {
                foe.CombatAI(PC, FoesList);
                //PC.UpdateState();
            }
            Scene.SetButtons(Scene.NextButton(FoeEndTurn));
        }
        #endregion



        #region CombatTurnStates
        public void StartCombat()
        {
            CombatMenu();
            CombatLong();
        }
        
        //Player Start of Turn
        public void PlayerStartTurn(object sender, EventArgs e) => PlayerStartTurn();
        public void PlayerStartTurn()
        {
            //Checks
            CombatMenu();
            //CombatLong();
        }

        //Player End of Turn
        public void PlayerEndTurn(object sender, EventArgs e) => PlayerEndTurn();
        public void PlayerEndTurn() {
            //Checks
            foeTurn();
        }
        //=======================================================================
        //Foe End of Turn
        public void FoeStartTurn(object sender, EventArgs e) => FoeStartTurn();
        public void FoeStartTurn()
        {
            //Checks
            foeTurn();
        }
        public void FoeEndTurn(object sender, EventArgs e) => FoeEndTurn();
        public void FoeEndTurn() {
            //Checks
            PlayerStartTurn();
        }
        #endregion
    }
}