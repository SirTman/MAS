﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAS.Actions
{
    public abstract class CombatAction
    {
        public string ActionName { protected set; get; }
        public bool RequiresTarget { protected set; get; } = true;
        public string RequiresPerk { protected set; get; }

        public bool IsRangedBased { protected set; get; } = false; // If set, use ranged combat calcs/lookups
        public bool IsMeleeBased { protected set; get; } = false; // Or melee

        //Values
        public int DmgHeal { protected set; get; }
        public DamageType DmgType { protected set; get; }
        public int ToHit { get; protected set; }
        public int SaveDC { protected set; get; }
        protected bool CRIT { set; get; } = false;

        protected void CritCal(int bonus) {
            if (ToHit >= (20 - bonus))
                CRIT = true;
            else
                CRIT = false;
        }
    }
}
