﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS.Items;

namespace MAS.Actions
{
    class WeaponAttack : CombatAction
    {

        public WeaponAttack(Item weapon, Character attacker) {
            try {
                if (!weapon.isWeapon) throw new Exception("Not a Weaon");

                var rand = new Random();
                ToHit = rand.Next(1,21);
                CritCal(weapon.CritBonus);
                ToHit += attacker.Reputation;
                if (weapon.Ranged)
                {
                    IsRangedBased = true;
                    //TODO: Thrown weapons can use eithe rstat //if (weapon.ItemProperties)
                    ToHit += attacker.RFX;
                    DmgHeal = attacker.RFX;

                }
                else if (weapon.Type == ItemType.Melee_Weapon)
                {
                    IsMeleeBased = true;
                    ToHit += attacker.STR;
                    DmgHeal = attacker.STR;
                }
                else { throw new Exception("Something Has gone wrong."); }

                DmgType = weapon.DamageType;
                if (CRIT) DmgHeal += (weapon.Damage * 2);
                else DmgHeal += weapon.Damage;
            }
            catch (Exception e) {
                MAS.MainForm.TextOutput = e.ToString();
            }
        }
        //Default PUNCH
        public WeaponAttack(Character attacker)
        {
            var rand = new Random();
            ToHit = rand.Next(1, 21) + attacker.STR;
            IsMeleeBased = true;

            if (CRIT) DmgHeal += (attacker.STR * 2);
            else DmgHeal += attacker.STR;
            DmgType = DamageType.BLUDGEONING;
            //Deals a Minimun of 1;
            if (DmgHeal < 0) DmgHeal = 1;
        }
    }
}
