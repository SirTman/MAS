﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAS;

namespace MAS
{
    public abstract class Creature
    {
        
        private static Random rnd = new Random();
        private string randomChoice(string[] str)
        {
            int r = rnd.Next(str.Length);
            return str[r];
        }

        public string Name { get; protected set; }
        public string Long { get; protected set; }//Long Description
        public string Species { get; set; }
        public Sex gender { get; protected set; }
        public bool HasFur { get; set; }

        public Face face = new Face();
        public Body body = new Body();

        //Core Stats
        private static int baseStat = 10;
        private int _constitutionScore;

        protected int StrengthScore { set; get; } //Strength,
        protected int ReflexScore { set; get; } //Reflex, 
        protected int ConstitutionScore { 
            set { _constitutionScore = value; SetHPMax(); } 
            get { return _constitutionScore; } 
        } //Constitution, 
        protected int CunningScore { set; get; } //Cunning, 
        protected int PresenceScore { set; get; } //Presence,
        protected int IntellectScore { set; get; } //Intellect

        //The Modifier e.g +1, -3, 0 etc.
        private int StatModifier(int score) {
            double scoreD = score;
            score = Convert.ToInt32(Math.Floor((scoreD - 10) / 2));
            return score;
        }
        public int STR { get { return this.StatModifier(StrengthScore); } }
        public int RFX { get { return this.StatModifier(ReflexScore); } }
        public int CON { get { return this.StatModifier(ConstitutionScore); } }
        public int CUN { get { return this.StatModifier(CunningScore); } }
        public int PRE { get { return this.StatModifier(PresenceScore); } }
        public int INT { get { return this.StatModifier(IntellectScore); } }


        ///Condition Stats
        private int _reputation = 1; 
        public int Reputation { get; set; }

        private double _libido = 0.0;   //Libido 0-100
        private double _senstivity = 1.0;   //Senstivity Multiplyer x0.0
        public double Senstivity { get; private set; }

        public double Lust { get; set; }    //Lust 
        private double _lustMax = 100.0; //Lust Maximun DFLT: 100
        public double LustMax { get; private set; }

        public int HP { get; protected set; } //Hit Points

        private int _hpMax;   //Hit Points Maximun
        public int HPmax { get { return _hpMax; } }

        public void SetHPMax() {
            _hpMax = _reputation + (this.CON * _reputation);
        }

        public string GetGender()
        {
            string t;
            switch (gender) {
                case Sex.male:
                    t = "male";
                    break;
                case Sex.female:
                    t = "female";
                    break;
                case Sex.andromorph:
                    t = "andromorph";
                    break;
                case Sex.gynomorphy:
                    t = "gynomorphy";
                    break;
                case Sex.herm:
                    t = "herm";
                    break;
                case Sex.maleherm:
                    t = "maleherm";
                    break;
                case Sex.undefined:
                    t = "unknown";
                    break;
                default:
                    t = "unknown";
                    break;
            }
            return t;
        }
        public int GetAbilityScore(Stat abl) {
            int score = 0;
            switch (abl) {
                case Stat.STR:
                    score = this.StrengthScore;
                    break;
                case Stat.RFX:
                    score = this.ReflexScore;
                    break;
                case Stat.CON:
                    score = this.ConstitutionScore;
                    break;
                case Stat.CUN:
                    score = this.CunningScore;
                    break;
                case Stat.PRE:
                    score = this.PresenceScore;
                    break;
                case Stat.INT:
                    score = this.IntellectScore;
                    break;
                default:
                    score = 0;
                    break;
            }
            return score;
        }
        public void SetAbilityScore(Stat abl, int num)
        {
            switch (abl) {
                case Stat.STR:
                    this.StrengthScore = num;
                    break;
                case Stat.RFX:
                    this.ReflexScore = num;
                    break;
                case Stat.CON:
                    this.ConstitutionScore = num;
                    SetHPMax();
                    break;
                case Stat.CUN:
                    this.CunningScore = num;
                    break;
                case Stat.PRE:
                    this.PresenceScore = num;
                    break;
                case Stat.INT:
                    this.IntellectScore = num;
                    break;
            }
        }
        public void AddAbilityScore(Stat abl, int num)
        {
            switch (abl) {
                case Stat.STR:
                    this.StrengthScore += num;
                    break;
                case Stat.RFX:
                    this.ReflexScore += num;
                    break;
                case Stat.CON:
                    this.ConstitutionScore += num;
                    SetHPMax();
                    break;
                case Stat.CUN:
                    this.CunningScore += num;
                    break;
                case Stat.PRE:
                    this.PresenceScore += num;
                    break;
                case Stat.INT:
                    this.IntellectScore += num;
                    break;
            }
        }
        public void SubAbilityScore(Stat abl, int num) {
            switch (abl)
            {
                case Stat.STR:
                    this.StrengthScore -= num;
                    break;
                case Stat.RFX:
                    this.ReflexScore -= num;
                    break;
                case Stat.CON:
                    this.ConstitutionScore -= num;
                    SetHPMax();
                    break;
                case Stat.CUN:
                    this.CunningScore -= num;
                    break;
                case Stat.PRE:
                    this.PresenceScore -= num;
                    break;
                case Stat.INT:
                    this.IntellectScore -= num;
                    break;
            }
        }

        ///Combat
        public void Damage(int number) {
            HP -= number; 
        }
        /// <summary>Deal X% of Maximum HP </summary>
        /// <param name="number">Whole Percantage</param>
        public void DamageScaled(int number)
        {
            HP -= (number / 100) * HPmax;
        }
        
        public void Heal(int number) {
            if ((HP + number) > HPmax) HP = HPmax;
            else HP += number;
        }
        /// <summary>Heals X% of Maximum HP </summary>
        /// <param name="number">Whole Percantage</param>
        public void HealScaled(int number)
        {
            if ((HP + number) > HPmax) HP = HPmax;
            else HP += number;
        }

        ///Descriptions
        public string HairLengthDescription() => face.HairLengthDescription(body.Height);
    }
}
