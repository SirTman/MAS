﻿using MAS.Creature;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAS
{
    class Beruch : Creature
    {
        public Beruch() {
            this.Name = "Beruch";
            this.Species = "Goblin";
            
            this.body.Height = 91.0;//cm
            this.body.Feminity = 30;
            this.body.Weight = 1.0;
            this.body.Thickness = 30;

            this.body.SetAllBustSize(6);
            this.body.CreateVagina();
            this.body.AssVol = 6;
            this.body.ThighsHips = 7;

            this.body.Skin = "skin";
            this.body.SkinTone = "green";
            this.body.SkinAdjective = "oily";

            this.face.HairColour = "auburn";
            this.face.HairStyle = "spiky";
            this.face.HairLength = 13;
            this.face.SetEyeColour("red");

            this.face.Ears = RacialType.ELVISH;
            this.face.EarsPier = "Pair of Silver Studs";

            this.StrengthScore = 8;
            this.ReflexScore = 14;
            this.ConstitutionScore = 10;
            this.CunningScore = 8;
            this.PresenceScore = 11;
            this.IntellectScore = 10;

            this.Long = "Yes";
        }

        public void FirstMeeting() {
            Globals.CurrentRoom.Output = "cheese";
        }
        
        private void attack() { }
    }
}
