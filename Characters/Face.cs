﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAS.Character
{
    public class Face
    {
        private static Random rnd = new Random();
        private string randomChoice(string[] str)
        {
            int r = rnd.Next(str.Length);
            return str[r];
        }

        //Hair
        public string HairColour { get; set; } //Hair Colour
        public string HairType { get; set; }   //Hair Type 
        public string HairStyle { get; set; }  //Hair Style 
        public double HairLength {get; set;}   //Hair Length (cm)

        private bool _beard = false;
        public bool Beard { get; set; } //If the character has a beard.
        public double BeardLength { get; set; } //Beard Length (cm)

        //Face
        public RacialType FaceType { get; set; }
        public RacialType Tounge { get; set; }
        public RacialType Ears { get; set; }
        public string EyesType { get; set; }
        private string[] _eyeColour = {"white", "white"};
       
        public string Antennae { get; set; }
        public string Horns { get; set; }
        public string HornsNum { get; set; } //Number of horns
        public double HornsMis { get; set; } //Might refer to the amount of branches if their antlers or length of horns

        //Piercings
        public string EarsPier { get; set; }
        public string NosePier { get; set; }
        public string LipPier { get; set; }
        public string ToungePier { get; set; }

        public Face() {
            this.FaceType = RacialType.HUMAN;
            this.Ears = RacialType.HUMAN;
            this.Tounge = RacialType.HUMAN;
        }
        public Face(RacialType type)
        {
            this.FaceType = type;
            this.Ears = type;
            this.Tounge = RacialType.HUMAN;
        }
        
        
        /// <summary> Sets both eyes to the same colour </summary>
        public void SetEyeColour(string colour) {
            _eyeColour[0] = colour;
            _eyeColour[1] = colour;
        }
        /// <summary> Sets each eye to the input colours colour </summary>
        /// <para name="colour1">Left Eye</para>
        /// <param name="colour2">Right Eye</param>
        public void SetEyeColour(string colour1, string colour2)
        {
            _eyeColour[0] = colour1;
            _eyeColour[1] = colour2;
        }
        /// <summary> Sets either the left or the right eye colour </summary>
        /// <param name="colour">Colour to set</param>
        /// <param name="setRight">if true it will set the Right eye, if False it will set the Left eye.</param>
        public void SetEyeColour(string colour, bool setRight)
        {
            _eyeColour[Convert.ToInt32(setRight)] = colour;
        }
        
        
        public void sethairLength(double val)
        {
            this.HairLength = val;
        }
        public string HairLengthDescription(double height)
        {
            string descript = "";
            List<string> adj = new List<string>();

            if (HairLength == 0) adj = new List<string> { "shaved", "bald", "smooth", "hairless", "glabrous" };
            else if (HairLength < 2.5) adj = new List<string> { "close-cropped", "trim" };
            else if (HairLength < 7.5) descript = "short";
            else if (HairLength < 15)
            {
                adj = new List<string> { "medium-length", "shaggy" };
                if (HairLength <= 10) adj.Add("ear-length");
                descript = randomChoice(adj.ToArray());
            }
            else if (HairLength < 25)
            {
                adj = new List<string> { "moderately long" };
                if (HairLength < 20) adj.Add("neck-length");
                else adj.Add("shoulder-length");
            }
            else if (HairLength < 40) adj = new List<string> { "long", "lengthy" };
            else if (HairLength < height / 2.5) adj = new List<string> { "very long", "back-length" };
            else if (HairLength < height / 1.6)
            {
                adj = new List<string> { "cascading" };
                if (HairLength < height / 1.7) adj.Add("ass-length");
                else adj.Add("thigh-length");
            }
            else if (HairLength < height / 1.3) adj = new List<string> { "delightfully long", "knee-length" };
            else if (HairLength < height)
            {
                adj = new List<string> { "exquisitely long" };
                if (HairLength < height - 1) adj.Add("calf-length");
                else adj.Add("ankle-length");
            }
            else adj = new List<string> { "floor-length", "obscenely long", "floor-dragging" };

            if ((adj != null) && (!adj.Any())) descript = randomChoice(adj.ToArray());
            return descript;
        }
    }
}
